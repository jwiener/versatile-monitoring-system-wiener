import re
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from models import Server,  Service,  Type,  Log


@api_view(['GET', 'POST'])
def home(request):
    if request.method == 'GET':
        return Response('GET NOT IMPLEMENTED', status=status.HTTP_400_BAD_REQUEST)

    if not ('server' in request.POST and 'type' in request.POST and 'log' in request.POST):
        return Response('INVALID REQUEST, PARAMS MISSING', status=status.HTTP_400_BAD_REQUEST)

    _server = str(request.POST.get('server'))
    _type = str(request.POST.get('type'))
    _log = str(request.POST.get('log'))

    try:
        _server = Server.objects.get(name=_server)
    except Server.DoesNotExist:
        return Response('INVALID SERVER %s' % _server, status=status.HTTP_400_BAD_REQUEST)

    try:
        _type = Type.objects.get(name=_type)
    except Type.DoesNotExist:
        return Response('INVALID TYPE %s' % _type, status=status.HTTP_400_BAD_REQUEST)

    try:
        _service = Service.objects.get(server=_server, type=_type)
    except Service.DoesNotExist:
        return Response('SERVICE DOES NOT EXISTS FOR SERVER %s AND TYPE %s' % (_server, _type),
                        status=status.HTTP_400_BAD_REQUEST)

    print 'server.name: ' + _server.name
    print 'type.name: ' + _type.name
    print 'service.validation_regex_test: ' + _service.validation_regex_test

    result = re.search(_log, _service.validation_regex_test)

    log = Log(service=_service, log=_log, result=bool(result))
    log.save()
    return Response('REQUEST SAVED', status=status.HTTP_200_OK)

#!/usr/bin/env python
import sys
import platform
import requests
from settings import LOG_SERVER_URL, LOG_SERVER_PORT
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    def handle(self, *args, **options):
        server = platform.node()
        if not self.isParamsValid(sys):
            #self.sendRequest('VEMOS PARAMETERS ERROR', server, 'unknown')
            print 'INVALID PARAMS'
            return

        lines = sys.stdin.readlines()
        if len(lines) == 0:
            #self.sendRequest('VEMOS PARAMETERS ERROR', server, 'unknown')
            print 'INVALID PARAMS'
            return

        log = '\n'.join(lines)
        args = sys.argv[2:]
        type = str(args[0])

        print 'log: ' + log
        print 'type: ' + type
        print 'server: ' + server

        request = self.sendRequest(log, server, type)
        print 'status_code: ' + str(request.status_code)
        print 'response: ' + str(request.text)
        return

    def sendRequest(self,  log, server, type):
        payload = {'log': log, 'server': server,  'type': type}
        req = 'http://' + LOG_SERVER_URL + ':' + LOG_SERVER_PORT + '/vemos'
        #print 'request: ' + req + '  ' + str(payload)
        return requests.post(req, data=payload)

    def isParamsValid(self, sys):
        if sys.stdin.isatty():
            return False

        if len(sys.argv) < 3:
            print 'argv error'
            return False
        return True

# End of vemos.py

#!/usr/bin/env python
from django.core.management.base import BaseCommand, CommandError
from models import Server,  Service,  Log,  Type
from django.core.mail import send_mail, BadHeaderError
from tabulate import tabulate
import datetime
import re
from settings import EMAIL_USE_TLS, EMAIL_HOST, EMAIL_HOST_USER,\
EMAIL_HOST_PASSWORD, EMAIL_PORT, EMAIL_ADRESSES, EMAIL_SUBJECT


class Command(BaseCommand):
    def handle(self, *args, **options):
        table = [[]]
        for server in Server.objects.all():
            row = []
            row.append(server.name)
            for type in Type.objects.all():
                service = Service.objects.filter(server=server, type=type)
                if len(service) != 0:
                    logs_today = Log.objects.filter(created__contains=datetime.date.today(), service=service)
                    if (len(logs_today) == 1):
                        m = re.search(service[0].validation_regex_test, logs_today[0].log,  re.I)
                        if m != None:
                            log_result = 'OK'
                        else:
                            log_result = 'ERROR'
                    else:
                        if len(logs_today) == 0:
                            log_result = 'MISSING'
                        else:
                            log_result = 'MULTIPLE LOGS'
                else:
                    log_result = '--'
                row.append(log_result)
            if len(table[0]) == 0:
                table[0] = row
            else:
                table.append(row)
        headers = ['Server']
        for type in Type.objects.all():
            headers.append(type.name)
        t = tabulate(table, headers=headers, stralign='left')
        print t
        send_mail(EMAIL_SUBJECT, t, 'example@gmail.com', EMAIL_ADRESSES)

# End of report.py

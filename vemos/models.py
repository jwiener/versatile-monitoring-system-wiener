from django.db import models
import datetime


class Server(models.Model):
    name = models.CharField(max_length=40, unique=True)
    description = models.TextField(blank=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'vemos'

    def __unicode__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=20, unique=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'vemos'

    def __unicode__(self):
        return self.name


class Service(models.Model):
    server = models.ForeignKey(Server)
    type = models.ForeignKey(Type)
    is_active = models.BooleanField()
    validation_regex_test = models.CharField(max_length=200)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'vemos'

    def __unicode__(self):
        return '%s (%s)' % (self.server.name, self.type.name)


class Log(models.Model):
    service = models.ForeignKey(Service)
    log = models.CharField(max_length=1024)
    result = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'vemos'

    def __unicode__(self):
        return '%s (%s)' % (self.service.id, self.log)
